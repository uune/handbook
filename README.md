# The Uune Handbook Repository

Hello there! You've stumbled across the repository for Uune's core handbook. This is where we keep the sauce and PDF builds. Feel free to poke around and check out some of the supplements we have to.

## Links

* Discord: https://discord.gg/gsXjCEtsNd
* Gitlab Organization: https://gitlab.com/uune

## Roadmap

Currently Uune is in Alpha. A lot has changed since I first released an alpha, but I think we're getting pretty close to a proper Beta release here soon. If you'd like to keep tabs on that, feel free to [check out the roadmap for it](https://gitlab.com/groups/uune/-/milestones/1).

## License

Everything here is licensed under the CC0 1.0 Universal License, aka as close to just saying "Public Domain" as I can get while still being legal in Europe.
