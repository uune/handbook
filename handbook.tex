\documentclass[12pt]{book}
\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
\usepackage[Sonny]{fncychap}

\renewcommand{\familydefault}{\sfdefault}

\newcommand{\Condition}[5] {
    \subsection{#1}
    \begin{description}
        \item[Cause:] #2
        \item[Effect:] #3
        \item[Stacking:] #4
        \item[Resolution:] #5
    \end{description}
}

\begin{document}
\frontmatter

\title{%
	\textbf{The Uune Handbook} \\
	\large{\textit{The Timefull Tabletop RPG}}
	}
\author{Benrob0329}
\maketitle

\begin{center}
To the extent possible under law, Benrob0329 has waived all copyright and related or neighboring rights to this work.

\hfill \\

\url{http://creativecommons.org/publicdomain/zero/1.0/}
\end{center}

\tableofcontents

\chapter{Preface: What is Uune?}

To put it simply, Uune is a game, where you and your friends all pretend to be other people, living in a fictional world, with one person guiding the rest of the table through said world in order to tell a story.

To be more specific though, Uune is my attempt to make a Tabletop Roleplaying Game that lets me write and run the games I want to play, in the way I want to play them.

\hfill \\

The goals of Uune are twofold: First and foremost, it's intended to be a game with a low barrier to entree, and a high ceiling of fun and creativity. Secondly though, it's meant to be a roleplay-first experience.

Accomplishing the first goal is really the overall structure of the project. Uune is designed to be a meta-rpg of sorts where the core mechanics, the real soul of the game, are shared across multiple branches. That is to say that the game, Uune, as contained in this handbook is not playable. Instead, you should grab a supplemental book containing additional mechanics, world info, and options after having read this handbook.

The second goal is a much harder problem though. So many RPGs are designed around combat being it's core mechanic, due to the hobby's heritage in the war-game genre. Take away that core crutch of conflict, and suddenly it's much harder to provide mechanics that give players agency and allow clever use of their resources. Once you get away from that rigid, combat focused formula too though, the classic ``save or suck'' style dice resolution doesn't make much sense either.

There is however a consistent source of stress and conflict, a universal constant which must always be fought against: Time. By treating Time as a core mechanic of the game, I can lean on the players to balance between using their resources to maximize their use of it, or to conserve them to take advantage of an abundance of it.

\hfill \\

The game has taken many, many forms to get to this point. From percentile dice and resolution tables, to static DCs and ``turnless combat'', Uune has been rethought and reworked time and time again to try to bring it into the form that fits my mental mapping for how this game should flow. The initial musing of ideas that would eventually become Uune actually began around the summer of 2020 in a Discord channel with my D\&D group. Among those ideas: A roleplay focused experience, spendable resources for players to use, and time as a core part of the game.

After about 4 years, a couple failed mini campaigns, and a lot of thinking and idea bouncing, I think Uune is finally in a state where it \textit{makes sense} as a game. Hopefully all of that effort results in something that's fun for everyone: players, writers, and game masters.


\mainmatter


\chapter{An Introduction To Role Playing Games}

Hello! And welcome to the world of Uune! In the sections that follow, I'll explain the parts of a typical Tabletop Roleplaying Game (TTRPG) as they pertain to Uune. I'll also be sprinkling in bits of advice whenever I can to help get your games off the ground.

In the later chapters I'll be going into more detail as to the rules and mechanics of Uune \emph{specifically,} but first we need to lay some groundwork so that you have a good foundation to work from.

\section{What is an RPG?}

Generally speaking, a Roleplaying Game is any game in which you, the player, take up the mantle (the \textit{role} if you will) of a specific character. Typically, this involves making choices which will affect the outcome of the game's story in some way, as well as the personal story of the character you control.

In the case of Tabletop (or Pen and Paper) RPGs specifically, it's a game where one player (known as the \textbf{Game Master}) runs the game for the rest of the table, usually with one character made and run by each player. The Game Master will generally have a rough plan for where the story goes, and much of the nitty-gritty details of the story are improvised between the interactions of the players, the environment, and other characters in the game.

\section{What do we play?}

Tabletop RPGs are more broadly called \emph{systems} when referring to the game itself with all of it's rules and mechanics. The base system defines how you interact with the story's world, usually called the \emph{setting}. In the case of Uune, these two things are separate. I like to call Uune a ``meta-RPG'' because of how I've structured it: you have The Handbook (this manual), which describes the overall concepts and frameworks that are used across all settings and games, with more specific things being a part of additional books called \emph{supplements}.

So essentially, when you ``Play Uune'', you're playing a game that builds on the base mechanics and concepts laid out here, with it's own tweaks and additions to help cement it into it's own feel and genre. Uune is designed to be adapted to a variety of game and story styles, while still keeping a familiar set of core ideas in order to greatly reduce the learning (and writing) curve when starting a new style of campaign.

\section{Who do we play?}

Back to something more concrete, you've probably noticed that I've mentioned playing ``characters'' a few times, but what exactly do I mean?

Well, there are two types of Characters in Uune: Main Characters and Side Characters.

\textbf{Main Characters} are those people which the story mostly focuses on. Each of these Characters is individually played by a single Player, which is why they are often called Player Characters by other games.

\textbf{Side Characters} are those people which the story does not focus on, but who are also a necessary part of it. All  these Characters are played by a single Player, the Game Master.

\textbf{The Game Master} is the head honcho of the table. They are mainly the eyes and ears of everyone else at the table, giving all of the Players a glimpse into the world of the story as seen through the perspective of their Character. The GM will also chaperon the story via elements under their control: the environment,  Side Characters, and rules. While each Main Character's decisions are entirely their Player's to make, the GM can reign in and veto courses of action which are impossible, advise against highly unlikely outcomes, and dictate the requirements (such as time and resources to spend) for things which are challenging but doable.

\section{How do we play?}

Usually, a game must first be planned before it can be played. The GM will often talk with Players ahead of time to get a feel for the types of Characters they want to play, the setting they want to play in, and what supplements they'll be playing with. Then, the GM can plan out a rough idea of what the story might look like, with somewhat more detail for things in the immediate future.  The Main Character's will be made with all of their abilities, personality, and other qualities. And finally, the first session will be played.

It's usually also a good idea to play a pre-game, a ``session zero'' if you will, to help cement everyone's idea of who their characters are and what the world looks like. It's also a good idea to get the approval and give a heads up for any sort of more mature subject matter or potentially off-putting content you might want to work into your story. There are far more qualified resources on these things than I can provide though, so a good search through the interwebs is probably your best bet.


\chapter{Everything About your Character}

Now that you've got a general idea of how RPGs (and specifically Uune) operate, I can get into the specifics of all the bits of information that make up your Character and what they can do.

\section{Your Backstory}

The first, and most important part of any Character is \textit{who they are.} We are all a product of our experiences, and the same goes for your Character. In general, a backstory should encompass their past, present, occupation, personal motives, hobbies, general appearance, friends, family, enemies, etc. Making sure you write a thorough backstory also makes it easier for your GM to pull elements of your Character's past into the game, helping to make them a part of the world around them.

Here are some questions to get you started:

\begin{itemize}
	\item What do they look like?
	\item What do they wear?
	\item Where do they come from?
	\item Who are their parents?
	\item Who is their best friend?
	\item What do they do for money?
	\item What is one thing they regret?
\end{itemize}

\section{The Character Sheet}

Next, you'll need a place to keep track of everything pertaining to your Character throughout the game. We call this a \textbf{Character Sheet}. You might have a premade Sheet from your GM or a Supplement that they're using, but you can easily make one with a notebook or word processor too if you need. I'll be going over the specifics of what everything on your Sheet is, what it means, and what you can do with it.

First, a good place to start is your Character's name. Some sheets might also have a place to put your backstory too, though I personally prefer to keep backstories separate as many of the people I've played with like to write very long backstories with lots of detail, and those just don't fit in a single backstory box.

\section{Tasks and Time}

Often, you'll want your Character to take action in some way to affect the situation. We call that a \textbf{Task}.

Tasks in Uune have a \textbf{Time} associated with them, which determines how long they'll take in-game to complete. Time is measured in minutes, and a Time of zero means that it's negligible, not instant. Sometimes you'll have a rule or mechanic that tells you how long something will take, but usually your GM will tell you based on the circumstance. If you do not have enough Time to complete your Task, or you lose the ability to spend Time on it, your Task cannot be completed. Thankfully though, there are several ways to reduce the amount of Time a given Task takes to complete, and I'll go into those below.

Tasks can also have a \textbf{Difficulty} associated with them. The Difficulty is just a way of quickly determining how long a Task will take. Your GM can roll 1d6 for each level of Difficulty, and add them together to get the time.

\section{Stamina and Risk}

\textbf{Stamina} is your Character's ability to push themselves and expend extra energy to get things done faster. You may spend a Stamina Die to reduce the time of any Task you have, rolling a six-sided-die (aka the d6) and subtracting the result from the Time you need. You can have 4 Stamina Dice, which are represented on your Character Sheet to help you keep track of them. You regain 1 Stamina Die for every 2 hours that your Character rests or sleeps.

In contrast, \textbf{Risk} is your Character's ability to cut corners and make quick decisions to speed things up. Risk works similarly to Stamina, where you can spend a Die and reduce the Time of any Task. Where it differs though is how you spend it. You don't have a limit on how much Risk you can take, but whenever you roll a Risk Die your GM must give your Character a consequence according to the table below, matched with the result of the die:

\hfill \\

\noindent
\begin{tabular}{ | l | l | }
	\hline
	1     & Something that hurts an object    \\ \hline
	2 - 3 & Something that hurts you          \\ \hline
	4 - 5 & Something that hurts someone else \\ \hline
	6     & The GM gets a point of Karma      \\
	\hline
\end{tabular}

\hfill \\

We'll go into what that last line means later, for now..don't worry about it.

\section{Levels, Skills, and Aspects}

\textbf{Levels} represent your Character's increase in experience and knowledge. Your GM grants your Characters a Level Up when it makes sense for the story. Whenever you gain a Level, you may choose either to put it into a Skill or an Aspect.

\textbf{Skills} are the representation of your Character's general experience with specific topics. Each game will have a different list of Skills you can have, as determined by both the Supplements you're using, as well as your GM. Skills can be something commonplace like driving, or specialized like lock picking or spell-casting.

Whenever you put a Level into a Skill, you gain a d6 that you can spend like Stamina on a Task pertaining to that Skill. Some Skills (usually because they're common knowledge) will also have a \textbf{Base Level}, which means that you start with that many Levels in the Skill for free.

You regain 1 Skill Die in all of your Skills for every 2 hours you sleep.

\hfill \\

Where Skills represent what you know, \textbf{Aspects} represent things about your Character. Similarly to Skills, the exact Aspects you'll have available will depend on your GM, but they usually pertain to things like influential events from their past, status in a guild, innate abilities, etc.

Whenever you use a Level to gain an Aspect, you can put it onto your Character Sheet. Many Aspects also have a perk, which you can now make use of.

\section{Aid and Encumbrance}

If your Character wishes to \textbf{Aid} someone else, they may give the Die from an ability (such as Stamina, Risk, or a Skill) to another Character, allowing them to subtract the result from their Task's Time.

Similarly, you can give \textbf{Encumbrance} to slow somebody down. Just like Aid, you can spend an ability Die, but the other Character must add to their Time instead.

In both cases, you must have a plausible way in which you can affect the other Character's Task, as determined by your GM.

\section{Luck Checks, Straight Rolls, and Quality Checks}

\textbf{Luck Checks} are a tool your GM can use to leave something up to chance, usually for the sake of quickly making a decision regarding the presence of an item, layout of a room, or other improvised detail. To make a Luck Check, phrase a question as a ``yes or no'' style sentence, then roll a d6. If the result is 4 or above, the answer is yes. Otherwise, the answer is no.

\textbf{Straight Rolls} are simply a term meaning to roll a d6 without anything affecting it. Usually this is used by specific mechanics in a supplement which need a roll that Characters can't change with any abilities, but it can also be used by GMs for their own purposes.

\textbf{Quality Checks} are a type of roll which is used to determine \textit{how well} your Character does something. Your GM can ask you to make one if there is a variable outcome for a Task, or something similarly applicable. If your GM calls for a Quality Check, make a Straight Roll and add +1 for every Die you spent on the Task.

\section{Crunch Time, Chases, and Attacks}

Tensions are rising, and things are starting to get hectic. Whenever there's a lot to keep track of, your GM may call for \textbf{Crunch Time}. CT is when you slow the game down to real time (or even slower) in order to keep track of everything that's going on. Usually, your GM will bring out a 1-minute game timer for Crunch Time, and begin going from person to person at the table to see what they're doing. Whenever the timer finishes, a minute has passed on \textit{everyone's} Task.

\textbf{Chases} are a rare scene in most RPGs, but they do come up nonetheless. The way I would run a Chase is to give the pursuers a Task with a Difficulty equal to the average number of Stamina Dice still available to the group being Chased. The group being Chased will need to Encumber the pursuers as much as possible and attempt to escape or hide before they can catch up.

\textbf{Attacks} on the other hand are fairly common in most RPGs, but not so much in Uune. Being mostly a roleplay focused game, Uune doesn't make use of combat nearly as much as most systems, and doesn't have separate rules catered to it. Instead, Attacks are more broad attempts to harm or kill someone, represented as a Task with a Difficulty equal to the number of Stamina Dice the attacked has left. The Attacker can then be Encumbered, and even Attacked themselves in an attempt to survive.

If there are a lot of Attacks going on, I recommend that the GM bring the game into Crunch Time to help keep everything sane.

\section{Equipment and Inventory}

Your Character will have item on their person, called \textbf{Equipment}. You can purchase Equipment from shops, characters, or be given it through means of the story. Certain Aspects may give you Equipment as part of taking it. Some Skills may also be intended to for use with a specific piece of Equipment, and will be required to use it properly.

To store Equipment, you'll need to put it into your \textbf{Inventory.} You have 5 Inventory ``slots'' you can make use of. Normal sized items will take up a single slot, with larger items potentially taking up more slots, or for very large items they may not be able to be stowed at all.

Anything in your main 5 slots can be accessed immediately, however you can also have other items which give you more storage space at the cost of convenience. For example, you might have a backpack which gives you an additional 10 slots, but will cost you 1 Minute to retrieve any items from it. Extra storage items also take up space in your Inventory, reducing the number of immediately accessible items you can have.

\section{Conditions}

\textbf{Conditions} are active effects which affect your Character. They are usually given out by your GM in response to either an environmental threat, or as consequence for a Risk roll or other course of action. The effects of Conditions included in the Handbook are laid out below:

\Condition{Battered}
    {Sustaining an injury or other debilitating effect.}
    {Every Task's Difficulty is increased by 1.}
    {Yes}
    {Whenever you sleep at least 6 hours, you heal 1 level of this Condition.}

\Condition{Drowning}
    {You can hold your breath for 30 seconds per Stamina Die you have left. After that, you will gain a level of this condition for every Minute that passes.}
    {You lose (i.e. spend with no benefit) 1 Stamina Die and gain a level of the Battered Condition.}
    {Yes}
    {Get out of the water!}

\Condition{Stunned}
    {Hitting your head hard, getting the wind knocked out of you, other mechanics as needed.}
    {You cannot start any Tasks, and any Tasks you had started are forfeit.}
    {No}
    {Resolves after 1 minute, or when another mechanic specifies.}

\Condition{Held}
    {Being tied up, grappled, etc}
    {You cannot move}
    {No}
    {You may attempt to break out like making an Attack.}

\Condition{Burning}
    {Extreme hot or cold}
    {You gain 2 levels of the Battered Condition for every Minute that passes.}
    {Yes}
    {You must take a minute to put yourself out, or escape from the extreme conditions.}



\chapter{A Fast and Loose Guide to GMing}

Now I'll go over some general rules, tables, and advise for running games and using the mechanics laid out so far. These aren't meant to be an all-encompassing guide to GMing, as there are far more qualified people than myself for giving that sort of advise, but hopefully I can give some insight into how I personally intend the game to be played, and you can chose whether to abide by that or not.

\section{Setting Task Difficulty}
Tasks are the bread of Uune's dice mechanics, while Time is the butter. To use one you'll need the other, and Difficulty is meant to help with that. Rather than determining the exact amount of Time something takes, just pick either about how long you think it'd probably take, or about how difficult it seems, and roll based on the table below:

\hfill \\

\begin{tabular}{| l | l | l |}
    \hline
    1 & Very Easy &  3 -  4 Minutes \\ \hline
    2 & Easy      &  6 -  8 Minutes \\ \hline
    3 & Less Easy &  9 - 12 Minutes \\ \hline
    4 & Medium    & 12 - 16 Minutes \\ \hline
    5 & Hard      & 15 - 20 Minutes \\ \hline
    6 & Very Hard & 18 - 24 Minutes \\
    \hline
\end{tabular}

\section{Side Characters, aka NPCs}
Almost every story is going to have Non-Player Characters in them, which is to say Characters that you control. These are built in largely the same way as Player Characters, though you don't need to write a lot (or any, for one-off Characters pulled out of a hat) when it comes to Backstory.

NPCs can make use of all the same abilities as normal PCs, though they needn't be limited to them. You can have custom abilities for monsters, villains, etc as you see fit. You can also chose to forgo any mechanical prep and have NPCs who have zero levels at all if they're purely a ``common folk'' or throw-away character.

\section{The Consequences of Risk}
Your players have a lot of tools in their arsenal to be able to get things done, but when push comes to shove there's one thing that they'll always be able to fall back on: Risk.

Taking a Risk is something that most players are going to avoid, just like they would in real life. However, it is in those moments of desperation that Risk can play a key part in letting the story continue, but leaving the Characters with more to deal with later.

The consequence table isn't meant to be taken super literally, ``hurt'' in this case generally means a negative effect of some kind. It can mean a literal injury (like the Battered Condition), but it can also mean that say, the Character sneaks past the guards \textit{but} is spotted by the king's daughter from afar. This can have varied consequences for the story, and may open up an opportunity to introduce another motive for an NPC.

You should choose consequences that make sense for the situation. It would be unfair to give a Character a major injury because they took a risk trying to talk to a noble, unless the noble happens to be partial to having his bodyguards beat the crap out of people he doesn't like.

And of course, the one consequence that is always certain is Karma, which I'll go into next.

\section{Karma: aka ``Because I Said So''}
As the GM, you hold general power over the world and NPCs, but don't have many excuses to directly influence the story. Simply making Tasks more difficult for your players because they've been rolling well and you need them to slow down seems cheap and unfair, and normally it would be.

But that's where Karma comes in.

Karma is your ticket to affect any rolls in the game however you please. Use it to Aid and Encumber the Tasks of your Players and NPCs like your own little pool of universal Stamina. Bargain with your players for it, and use it as leverage whenever you have it.

Say you have a party of religious fighters who have been acting a little bit like a bunch of murder-hobos. Karma don't lie, so use it to make those behaviors more difficult. Has the party been struggling with rolls tonight and needs a hand? The odds can be more in their favor, should they need it. It's all up to you, so have some fun with it.

\section{When to use...}
Here is some general advise for when and where to use some of the Game Mastery tools you have available, in a quick-fire and easy-to-reference kind of way:

\begin{description}
    \item[Tasks] are generally used whenever Characters do something that \textit{isn't} inconsequential. Actions like walking across an open room, opening an unlocked door, or casually talking to an NPC don't need, and probably shouldn't use a Task. Meanwhile, actions like picking a lock, traversing an obstacle course, or attempting a diplomatic negotiation should most likely make use of Tasks.

    \item[Quality Checks] should be used whenever you need to know \textit{how well} a Character does something, such as a negotiation, an attempt to hide, or when crafting a complex item. QCs are a relative scale, so determine what the best and worst case outcomes are first, then have the player roll to see where on that scale they lie.

    \item[Luck Checks] are useful for when you need a small detail (with potentially big impact) decided but don't have a clear path to determine that. I've used these for determining if players remembered to take a small item not explicitly in their inventory, or if a room layout is favorable to the player's wishes. You can even use these for on-the-fly NPC improv, such as seeing what side of an argument someone takes, which side of their face a notable scar is on, etc.

    \item[Conditions] while rare, are meant to provide a clear path for what to do in specific circumstances. They're use is largely intended to be for when there should be mechanical consequence to an action or event, rather than just roleplaying the scene and ``softballing'' it.

    \item[Level Ups] should be given whenever you want your Characters to have a bump in ability, or when they've done something to train or further their story in a significant way. It's really up to you, there aren't any right or wrong ways to level your party, just when it feels right.

    \item[Karma] is your window into fate, allowing you to influence freely whenever you have it available to you. Karma can be used to help your players out if they're struggling, or to hold them back if they need a little extra difficulty. You can use it to reinforce good morals (a cosmic interpretation of karma), or simply as a means of guiding the story as you see fit.

    \item[Crunch Time] is mainly intended to be used in a combat-like setting, but can also be useful for other complex situations. It's there for your sanity, so use it whenever you need to breathe while still moving things along.
\end{description}

\end{document}
